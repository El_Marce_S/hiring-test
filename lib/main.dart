import 'package:flutter/material.dart';
import 'package:hiring_test/Providers/AppState.dart';
import 'package:provider/provider.dart';
import 'Config/RouteGenerator.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => AppState()),
    ],
    child: InitPage._(),
  ));
}

class InitPage extends StatefulWidget {
  InitPage._();

  @override
  _InitPageState createState() => _InitPageState();
}

class _InitPageState extends State<InitPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        onGenerateRoute: RouteGenerator.generateRoute,
        initialRoute: "/",
        builder: (context, child) {
          return MediaQuery(
            child: child as Widget,
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          );
        },
      ),
    );
  }
}
