import 'package:flutter/material.dart';

class AppState with ChangeNotifier {
  List _filteredUsers = [];

  get GetFilteredUsers {
    return _filteredUsers;
  }

  set SetFilteredUsers(NewUsersList) {
    _filteredUsers = NewUsersList;
    notifyListeners();
  }
}
