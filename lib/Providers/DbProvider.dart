import 'dart:io';

import 'package:hiring_test/Models/User%20Model.dart';
export 'package:hiring_test/Models/User%20Model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database ??= await initDB();
    return _database;
  }

  Future<Database> initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'Users.db');
    print(path);

    return await openDatabase(
      path,
      version: 2,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute('''
        CREATE TABLE Users(
          id INTEGER PRIMARY KEY,
          name TEXT,
          phone TEXT,
          email TEXT
        )
        ''');
      },
    );
  }

  //Check if ID Exist
  CheckIfIDExist(UserModel NuevoUsuario) async {
    print('El ID a buscar');
    print(NuevoUsuario.id);
    final id = NuevoUsuario.id;
    final db = await database;

    final res = await db!.rawQuery('''
    SELECT * FROM Users WHERE id = '$id'
    ''');
    return res;
  }

  //Crear Registros en la DB
  NewUserRecordRAW(UserModel NuevoUsuario) async {
    print('el usuario a registrar');
    print(NuevoUsuario);
    final id = NuevoUsuario.id;
    final name = NuevoUsuario.name;
    final email = NuevoUsuario.email;
    final phone = NuevoUsuario.phone;

    final db = await database;

    final res = await db!.rawInsert('''
    INSERT INTO Users( id, name,  email, phone)
    VALUES ($id, '$name', '$email', '$phone')
    ''');
    return res;
  }

  GetAllUsers() async {
    final db = await database;
    final res = await db!.rawQuery("SELECT * FROM Users");
    return (res);
  }
}
