import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hiring_test/Pages/PostsPage.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class UserCard extends StatelessWidget {
  final id;
  final name;
  final phone;
  final email;

  const UserCard({Key? key, this.id, this.name, this.phone, this.email})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 5,left: 10, right: 10),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.9,
        child: Card(
          elevation: 15,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TextStyle(
                      color: Color.fromRGBO(55, 93, 55, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.phone,
                      color: Color.fromRGBO(55, 93, 55, 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        phone,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Icon(
                      CupertinoIcons.envelope,
                      color: Color.fromRGBO(55, 93, 55, 1),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Text(
                        email,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 15),
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: MaterialButton(
                    color: Color.fromRGBO(55, 93, 55, 1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0),
                    ),
                    child: Text(
                      'Ver Publicaciones',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      showCupertinoModalBottomSheet(
                        context: context,
                        builder: (context) => PostsPage(name: name, id: id),
                      );
                      print('El Id del Usuario es:' + id.toString());
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
