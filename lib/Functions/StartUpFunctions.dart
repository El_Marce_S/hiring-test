import 'package:hiring_test/Providers/DbProvider.dart';

CheckIfUserExist({userData}) async {
  var IdExist = await DBProvider.db.CheckIfIDExist(userData);
  print(IdExist.length);
  if (IdExist.length == 0) {
    print('en caso de no existir ID debemos guardar');
    DBProvider.db.NewUserRecordRAW(userData);
  } else {
    return null;
  }
}
