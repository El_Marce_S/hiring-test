import 'dart:convert';

import 'package:hiring_test/Config/ApiUrl.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

var logger = Logger();

GetAllUsers() async {
  var url = Uri.parse(ApiUrl() + 'users');
  final responseStatus;
  Map<String, String> header = {
    'Content-type': 'application/json; charset=utf-8',
    'Accept': 'application/json',
  };
  try {
    http.Response response = await http.get(
      url,
      headers: header,
    );
    responseStatus = await response.statusCode;
    print('el estado');
    print(responseStatus.runtimeType);
    if (responseStatus==200) {
      final decodedData = json.decode(response.body);
      return decodedData;
    } else {
      print('pasando a los datos locales');
      return false;
    }
  } catch (e) {
    logger.wtf('something happened');
    logger.wtf(e);
  }
}

GetAllPostsByUserID({id}) async {
  print('realizadno request de informacion');
  var url = Uri.parse(ApiUrl() + 'posts?userId=' + id.toString());
  print('Imprimiremos la URL');
  print(url);
  Map<String, String> header = {
    'Content-type': 'application/json; charset=utf-8',
    'Accept': 'application/json',
  };
  http.Response response = await http.get(
    url,
    headers: header,
  );

  final decodedData = json.decode(response.body);
  print('El Estado del request');
  print(response.statusCode);
  return decodedData;
}
