import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:hiring_test/Components/PostsPage/PostCard.dart';
import 'package:hiring_test/Functions/DataProvider.dart';

class PostsPage extends StatelessWidget {
  final name;
  final id;

  const PostsPage({Key? key, this.name, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(126, 162, 170, 1),
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Text(
                'Post Hechos por ',
                style: TextStyle(fontSize: 15),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              child: Text(
                name,
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      child: FutureBuilder(
                        future: GetAllPostsByUserID(id: id),
                        builder:
                            (BuildContext context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData) {
                            var data = snapshot.data;
                            print('la inforcioan recibida');
                            print(data);
                            return ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: data.length,
                              itemBuilder: (BuildContext context, int index) {
                                var _article = data[index];
                                var _title = _article['title'];
                                var _body = _article['body'];
                                return FadeInRight(
                                    delay: Duration(milliseconds: index * 500),
                                    child:
                                        PostCard(title: _title, body: _body));
                              },
                            );
                          } else {
                            return SpinKitWave(
                              color: (Color.fromRGBO(55, 93, 55, 1)),
                              size: 50.0,
                            );
                            ;
                          }
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text('Atras'),
        backgroundColor: Color.fromRGBO(55, 93, 55, 1),
      ),
    );
  }
}
