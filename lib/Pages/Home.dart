import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:hiring_test/Components/HomePage/SearchBar.dart';
import 'package:hiring_test/Components/HomePage/UserCard.dart';

class HomePage extends StatefulWidget {
  final StoredUsers;

  const HomePage({Key? key, this.StoredUsers}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List users = [];
  String query = '';

  @override
  void initState() {
    super.initState();
    users = widget.StoredUsers;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(240, 240, 201, 1),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(55, 93, 55, 1),
        title: Text(
          'Prueba de Ingreso',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ),
      body: Column(
        children: [
          buildSearch(),
          users.isNotEmpty
              ? Expanded(
                  child: ListView.builder(
                    itemCount: users.length,
                    itemBuilder: (context, index) {
                      var user = users[index];
                      return FadeInRight(
                        child: UserCard(
                            id: user['id'],
                            name: user['name'],
                            phone: user['phone'],
                            email: user['email']),
                      );
                    },
                  ),
                )
              : Container(
                  child: Card(
                    margin: EdgeInsets.all(30),
                    elevation: 20,
                    child: Padding(
                      padding: const EdgeInsets.all(40.0),
                      child: Text(
                        'La Lista esta vacía o no existen usuarios con esos parametros',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Color.fromRGBO(55, 93, 55, 1),
                        ),
                      ),
                    ),
                  ),
                )
        ],
      ),
    );
  }

  buildSearch() => SearchBar(
        text: query,
        hintText: 'Escriba el numbre de un Usuario',
        onChanged: searchUser,
      );

  void searchUser(String query) {
    List _users;
    _users = widget.StoredUsers;
    final usersfiltered = _users.where((user) {
      final nameLower = user['name'].toLowerCase();
      final searchLower = query.toLowerCase();
      return nameLower.contains(searchLower);
    }).toList();
    if (query == '') {
      setState(() {
        this.query = query;
        this.users = widget.StoredUsers;
      });
    } else {
      setState(() {
        this.query = query;
        this.users = usersfiltered;
      });
    }
  }
}
