import 'dart:convert';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:hiring_test/Functions/DataProvider.dart';
import 'package:hiring_test/Functions/StartUpFunctions.dart';
import 'package:hiring_test/Providers/AppState.dart';
import 'package:hiring_test/Providers/DbProvider.dart';
import 'package:provider/provider.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppState>(context);
    Future.delayed(Duration(milliseconds: 4000), () async {
      await DBProvider.db.database;
      var _allUsers = await GetAllUsers();
      if (_allUsers != false) {
        final _singleUser;
        for (_singleUser in _allUsers) {
          final _newUser = new UserModel(
            id: _singleUser['id'],
            name: _singleUser['name'],
            email: _singleUser['email'],
            phone: _singleUser['phone'],
          );
          await CheckIfUserExist(userData: _newUser);
        }
      }
      var storedUsers = await (DBProvider.db.GetAllUsers());
      Navigator.of(context).pushNamedAndRemoveUntil(
          '/home', (Route<dynamic> route) => false,
          arguments: storedUsers);
    });

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FadeInRight(
            child: Container(
              height: MediaQuery.of(context).size.height * 0.4,
              child: Image.asset('assets/icons/DevLogo.png'),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Center(
            child: Text('Obteniendo Información del Servidor'),
          ),
        ],
      ),
    );
  }
}
