import 'package:flutter/material.dart';
import 'package:hiring_test/Pages/Home.dart';
import 'package:hiring_test/Pages/SplashScreen.dart';
import 'package:page_transition/page_transition.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    print("ruta --->");
    print(settings.name);
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (_) => Splash(),
        );
        break;
      case '/home':
        var _users = settings.arguments;
        return PageTransition(
            child: HomePage(StoredUsers: _users),
            type: PageTransitionType.fade,
            settings: settings);
        break;
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
        break;
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('Pagina No Encontrada Posiblemente este en Desarrollo'),
        ),
      );
    });
  }
}
